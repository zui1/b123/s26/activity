let http = require("http");

http.createServer(function(req,res){

	if(req.url === "/"){

	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end('Welcome to Our Page');

} else if(req.url === "/login"){

	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end('Welcome to the Login Page. Please log in your credentials.');

} else if(req.url === "/register"){

	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end('Welcome to the Register Page. Please register your details.');

} else {

	res.writeHead(404,{'Content-Type': 'text/plain'})
	res.end('Resource Not Found.')

}

}).listen(8000);

console.log(`Server is running at localHost:8000`)